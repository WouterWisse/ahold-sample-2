//
//  AholdSampleUITests.swift
//  AholdSample2UITests
//
//  Created by Wouter Wisse on 07/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest

class AholdSampleUITests: XCTestCase {
    
     var testApp = XCUIApplication()
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        self.testApp.launchArguments.append("UITest")
        self.testApp.launch()
    }
    
    func testAholdSampleApp() {
        let app = self.testApp
        app.buttons["Load news"].tap()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Belastingtelefoon uit de lucht door storing"]/*[[".cells.staticTexts[\"Belastingtelefoon uit de lucht door storing\"]",".staticTexts[\"Belastingtelefoon uit de lucht door storing\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        XCTAssertTrue(app.staticTexts["Belastingtelefoon uit de lucht door storing"].exists, "Title should exist")
        XCTAssertTrue(app.staticTexts["Dat is al voor de tweede keer in een week tijd."].exists, "Description should exist")
        XCTAssertTrue(app.staticTexts["Telegraaf.nl - John Doe"].exists, "Author should exist")
        XCTAssertTrue(app.staticTexts["maandag 6 augustus 2018"].exists, "Date should exist in Dutch")
        
        app.navigationBars["Select Category"].buttons["News"].tap()
        app.navigationBars["News"].buttons["Select Category"].tap()
    }
}
