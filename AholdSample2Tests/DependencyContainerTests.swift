//
//  DependencyContainerTests.swift
//  AholdSample2Tests
//
//  Created by Wouter Wisse on 07/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample2

class DependencyContainerTests: XCTestCase {
    
    var factory: DependencyContainer!
    var mockFactory: DependencyContainerMock!
    
    override func setUp() {
        super.setUp()
        
        self.factory = DependencyContainer()
        self.mockFactory = DependencyContainerMock()
    }
    
    func testFactory() {
        XCTAssertTrue(self.factory.makeDefaultAPIService() is NewsAPIService, "Default Factory should return service of type NewsAPIService")
    }
    
    func testMockFactory() {
        XCTAssertTrue(self.mockFactory.makeDefaultAPIService() is NewsAPIServiceMock, "Default Factory should return service of type NewsAPIServiceMock")
    }
}
