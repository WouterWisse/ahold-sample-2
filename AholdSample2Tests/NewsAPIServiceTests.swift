//
//  NewsAPIServiceTests.swift
//  AholdSample2Tests
//
//  Created by Wouter Wisse on 07/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample2

class NewsAPIServiceTests: XCTestCase {
    
    private var newsAPIService: NewsAPIService!
    
    override func setUp() {
        super.setUp()
        let mockFactory = DependencyContainerMock()
        self.newsAPIService = mockFactory.makeDefaultAPIService()
    }
    
    func testNewsAPIService() {
        self.newsAPIService.executeRequest { (articleList, error) in
            
            XCTAssertNotNil(articleList, "Articles should be not nil.")
            XCTAssertNil(error, "Error should be nil.")
            
            guard let articleList = articleList else { return }
            XCTAssertTrue(articleList.articles.count == 1, "Article count should be one.")
        }
    }
}
