//
//  ArticleViewModelTest.swift
//  AholdSample2Tests
//
//  Created by Wouter Wisse on 07/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample2

class ArticleViewModelTest: XCTestCase {
    
    var articleWithFullSource: Article!
    var articleWithHalfSource: Article!
    
    override func setUp() {
        super.setUp()
        self.articleWithFullSource = Article(source: Source(id: "1", name: "Apple"),
                                                           author: "John Appleseed",
                                                           title: "iPhone XS coming soon!",
                                                           description: "It's finally there. The smaller iPhone X. The iPhone XS.",
                                                           url: nil,
                                                           urlToImage: nil,
                                                           publishedAt: "2018-08-06T16:59:32Z")
        
        self.articleWithHalfSource = Article(source: Source(id: "1", name: "Apple"),
                                             author: nil,
                                             title: "iPhone XS coming soon!",
                                             description: "It's finally there. The smaller iPhone X. The iPhone XS.",
                                             url: nil,
                                             urlToImage: nil,
                                             publishedAt: "2018-08-06T16:59:32Z")
    }

    func testArticleViewModelWithHalfSource() {
        let viewModel = ArticleViewModel(model: self.articleWithHalfSource)
        XCTAssertTrue(viewModel.model.fullSource == "Apple", "Full Source should match")
    }
    
    func testArticleViewModelWithFullSource() {
        let viewModel = ArticleViewModel(model: self.articleWithFullSource)
        XCTAssertTrue(viewModel.model.fullSource == "Apple - John Appleseed", "Full Source should match")
    }
    
    func testViewModelDateFormatterOutput() {
        let viewModel = ArticleViewModel(model: self.articleWithFullSource)
        XCTAssertTrue(viewModel.model.publishDateString == "maandag 6 augustus 2018", "DateString should match in Dutch")
    }
}
