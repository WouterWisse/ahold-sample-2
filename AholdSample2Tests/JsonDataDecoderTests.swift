//
//  JsonDataDecoderTests.swift
//  AholdSample2Tests
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import XCTest
@testable import AholdSample2

class JsonDataDecoderTests: XCTestCase {
        
    var jsonDataDecoder: JsonDataDecoder<Articles>!
    
    override func setUp() {
        super.setUp()
        
        let path = Bundle.main.path(forResource: "mock", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        self.jsonDataDecoder = JsonDataDecoder(data: data)
    }
    
    func testArticleListNotEmpty() {
        do {
            let articleList: Articles = try self.jsonDataDecoder.loadData()
            XCTAssertFalse(articleList.articles.isEmpty, "Article List should not be empty.")
        } catch {
            XCTFail("Article List could not be parsed.")
        }
    }
    
    func tesArticleContainsThreeUsers() {
        do {
            let articleList: Articles = try self.jsonDataDecoder.loadData()
            XCTAssertTrue(articleList.articles.count == 20, "Article List should contain 20 articles.")
        } catch {
            XCTFail("Article List could not be parsed.")
        }
    }
    
}
