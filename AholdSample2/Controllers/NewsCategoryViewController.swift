//
//  NewsCategoryViewController.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

class NewsCategoryViewController: UIViewController {
    typealias Factory = ViewControllerFactory & NewsAPIServiceFactory & NewsAPIRequestFactory
    
    // MARK: - Private Properties
    
    private var factory: Factory
    private var selectedCategory: NewsAPICategory = .general
    
    private lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(pickerView)
        return pickerView
    }()
    
    private lazy var goButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Load news", for: .normal)
        button.addTarget(self, action: #selector(goButtonTapped(sender:)), for: .touchUpInside)
        self.view.addSubview(button)
        return button
    }()
    
    // MARK: - Initializer
    
    init(factory: Factory) {
        self.factory = factory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select Category"
        self.setupLayout()
    }
    
    // MARK: - Private Functions
    
    private func setupLayout() {
        self.view.backgroundColor = UIColor.white
        NSLayoutConstraint.activate([
            self.pickerView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.pickerView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            self.goButton.centerXAnchor.constraint(equalTo: self.pickerView.centerXAnchor),
            self.goButton.topAnchor.constraint(equalTo: self.pickerView.bottomAnchor, constant: 16.0)
        ])
    }
    
    @objc func goButtonTapped(sender: UIButton) {
        let apiRequest = self.factory.makeAPIRequest(section: "top-headlines", country: "nl", category: self.selectedCategory)
        let apiService = self.factory.makeDefaultAPIService(with: apiRequest)
        let newsTableViewController = self.factory.makeNewsTableViewController(apiService: apiService)
        self.navigationController?.pushViewController(newsTableViewController, animated: true)
    }
}

// MARK: - UIPickerViewDataSource

extension NewsCategoryViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return NewsAPICategory.all.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return NewsAPICategory.all[row].rawValue.capitalized
    }
}

// MARK: - UIPickerViewDelegate

extension NewsCategoryViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCategory = NewsAPICategory.all[row]
    }
}
