//
//  NewsTableViewController.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController {
typealias Factory = ViewControllerFactory & NewsAPIServiceFactory & NewsAPIRequestFactory
    
    // MARK: - Private Properties
    
    private var factory: Factory
    private var apiService: NewsAPIService
    
    private var articles: [Article] = [Article]()
    private let cellReuseIdentifier: String = "CellIdentifier"
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            ])
        
        return activityIndicator
    }()
    
    // MARK: - Initializer
    
    convenience init(factory: Factory, apiService: NewsAPIService) {
        self.init(style: .plain)
        self.factory = factory
        self.apiService = apiService
    }
    
    override init(style: UITableViewStyle) {
        self.factory = DependencyContainer()
        self.apiService = self.factory.makeDefaultAPIService()
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "News"
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
        
        self.activityIndicator.startAnimating()
        self.apiService.executeRequest { [weak self] (articles, error) in
            if error != nil {
                print("Error: \(String(describing: error?.localizedDescription))")
            } else {
                if let articles = articles?.articles {
                    DispatchQueue.main.async {
                        self?.reloadTableViewWith(articles: articles)
                    }
                }
            }
        }
    }
    
    // MARK: - Private Functions
    
    private func reloadTableViewWith(articles: [Article]) {
        self.articles = articles
        self.activityIndicator.stopAnimating()
        self.tableView.reloadData()
    }
}

// MARK: - TableView DataSource

extension NewsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier, for: indexPath)
        let article = self.articles[indexPath.row]
        
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = article.title
        
        return cell
    }
}

// MARK: - TableView Delegate

extension NewsTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = self.articles[indexPath.row]
        let articleViewController = self.factory.makeArticleViewController(with: article)
        self.navigationController?.pushViewController(articleViewController, animated: true)
    }
}
