//
//  ArticleViewController.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {
    typealias Factory = ImageLoadFactory
    
    // MARK: - Private Properties
    
    private var article: Article
    private var factory: Factory
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8.0
        self.view.addSubview(stackView)
        return stackView
    }()
    
        // MARK: - Initialize
    
    init(article: Article, factory: Factory) {
        self.article = article
        self.factory = factory
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.addContent(for: ArticleViewModel(model: self.article))
    }
    
    // MARK: - Private Functions
    
    private func setupLayout() {
        self.view.backgroundColor = UIColor.white
        NSLayoutConstraint.activate([
            self.stackView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: 16),
            self.stackView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16),
            self.stackView.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor),
            self.stackView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16),
            ])
    }
    
    private func addLabelToStackView(with text: String?, fontTextStyle: UIFontTextStyle) {
        guard let text = text else { return }
        let label = UILabel()
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: fontTextStyle)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        self.stackView.addArrangedSubview(label)
    }
    
    private func addContent(for article: ArticleViewModel) {
        self.addImage(from: article.model)
        self.fillContent(from: article.model)
        self.fillSource(from: article.model)
        self.fillDate(from: article.model)
    }
    
    private func addImage(from model: ArticleImageProvider) {
        if let imageUrl = model.urlToImage {
            self.addImageView(for: imageUrl)
        }
    }
    
    private func fillContent(from model: ArticleContentProvider) {
        self.addLabelToStackView(with: model.title, fontTextStyle: .headline)
        self.addLabelToStackView(with: model.description, fontTextStyle: .body)
    }
    
    private func fillDate(from model: ArticleDateProvider) {
        self.addLabelToStackView(with: model.publishDateString, fontTextStyle: .footnote)
    }
    
    private func fillSource(from model: ArticleSourceProvider) {
        self.addLabelToStackView(with: model.fullSource, fontTextStyle: .footnote)
    }
    
    private func addImageView(for url: String) {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        self.stackView.addArrangedSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: 200.0)
            ])
        
        let imageLoader = self.factory.makeImageLoader(url: url)
        imageLoader.loadImage { (image, error) in
            DispatchQueue.main.async {
                imageView.image = image
            }
        }
    }
}
