//
//  ArticleViewModel.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation
import UIKit

struct ArticleViewModel {
    typealias Model = ArticleSourceProvider & ArticleContentProvider & ArticleImageProvider & ArticleDateProvider
    let model: Model
    
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "nl")
        return dateFormatter
    }()
    
    init(model: Model) {
        self.model = model
    }
}

protocol ArticleSourceProvider {
    var source: Source { get }
    var sourceName: String? { get }
    var author: String? { get }
    var fullSource: String { get }
}

extension ArticleSourceProvider {
    
    var sourceName: String? {
        return self.source.name
    }
    
    var fullSource: String {
        var source = ""
        if let sourceName = self.sourceName, sourceName.isEmpty == false {
            source += sourceName
        }
        if let auhorName = self.author, auhorName.isEmpty == false {
            source += (source.isEmpty ? "" : " - ") + auhorName
        }
        
        return source
    }
}

protocol ArticleContentProvider {
    var title: String? { get }
    var description: String? { get }
}

protocol ArticleImageProvider {
    var urlToImage: String? { get }
}

protocol ArticleDateProvider {
    var publishedAt: String? { get }
    
    var publishDate: Date? { get }
    var publishDateString: String? { get }
}

extension ArticleDateProvider {
    
    var publishDate: Date? {
        guard let publishedAt = self.publishedAt else { return nil }
        ArticleViewModel.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return ArticleViewModel.dateFormatter.date(from: publishedAt)
    }
    
    var publishDateString: String? {
        guard let publishDate = self.publishDate else { return nil }
        ArticleViewModel.dateFormatter.dateStyle = .full
        return ArticleViewModel.dateFormatter.string(from: publishDate)
    }
}
