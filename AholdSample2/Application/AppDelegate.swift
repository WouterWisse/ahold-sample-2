//
//  AppDelegate.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
typealias Factory = ViewControllerFactory
    
    var window: UIWindow?
    
    var isRunningUnitTests: Bool {
        return ProcessInfo().arguments.contains("UITest")
    }
    
    private lazy var factory: Factory = {
        
        if self.isRunningUnitTests {
            return DependencyContainerMock()
        } else {
            return DependencyContainer()
        }
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow()
        
        let newsCategory = self.factory.makeNewsCategoryViewController()
        self.window?.rootViewController = UINavigationController(rootViewController: newsCategory)
        self.window?.makeKeyAndVisible()
        
        return true
    }
}
