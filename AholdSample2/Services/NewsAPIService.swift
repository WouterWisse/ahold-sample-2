//
//  NewsAPIService.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

class NewsAPIService {
    typealias RequestCompletionHandler = (_ result: Articles?, _ error: Error?) -> Void
    
    // MARK: - Private Properties
    
    private var baseURL: String
    private var apiKey: String
    
    // MARK: - Internal Properties
    
    var request: NewsAPIRequest?
    
    // MARK: - Initialize
    
    init(baseURL: String, apiKey: String) {
        self.baseURL = baseURL
        self.apiKey = apiKey
    }
    
    // MARK: - Internal Functions
    
    func executeRequest(completion: RequestCompletionHandler?) {
        guard let request = self.request else { return }
        
        let urlString = self.baseURL + request.section + "?country=" + request.country + "&category=" + request.category.rawValue + "&apiKey=" + self.apiKey
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if data != nil {
                let jsonDecoder = JsonDataDecoder<Articles>(data: data)
                do {
                    let articles = try jsonDecoder.loadData()
                    completion?(articles, nil)
                } catch {
                    print("Decoding json failed with error: \(error.localizedDescription)")
                    completion?(nil, error)
                }
            } else {
                print("Error while executing request: \(String(describing: error?.localizedDescription))")
                completion?(nil, error)
            }
        }
        task.resume()
    }
}

enum NewsAPICategory: String {
    case general, business, entertainment, health, science, sports, technology
    static let all: [NewsAPICategory] = [.general, .business, .entertainment, .health, .science, .sports, .technology]
}

class NewsAPIRequest {
    var section: String
    var country: String
    var category: NewsAPICategory
    
    init(section: String, country: String, category: NewsAPICategory) {
        self.section = section
        self.country = country
        self.category = category
    }
}
