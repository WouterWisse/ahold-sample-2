//
//  JsonDataDecoder.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

internal enum JsonDataDecoderError: Error {
    case unknownError
    case noDataError
    case decodingError(description: String)
}

internal class JsonDataDecoder<T: Decodable> {
    
    // MARK: - Private Properties
    
    private var data: Data?
    
    // MARK: - Initialize
    
    init(data: Data? = nil) {
        self.data = data
    }
    
    // MARK: - Internal Functions
    
    func loadData() throws -> T  {
        guard let data = self.data else { throw JsonDataDecoderError.noDataError }
        
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            throw JsonDataDecoderError.decodingError(description: error.localizedDescription)
        }
    }
}
