//
//  ImageLoader.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation
import UIKit

class ImageLoader {
    typealias ImageLoaderCompletionHandler = (_ result: UIImage?, _ error: Error?) -> Void
    
    // MARK: - Private Properties
    
    private var imageURL: String
    
    // MARK: - Initialize
    
    init(imageURL: String) {
        self.imageURL = imageURL
    }
    
    // MARK: - Internal Functions
    
    func loadImage(completion: ImageLoaderCompletionHandler?) {
        guard let url = URL(string: self.imageURL) else { return }
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let data = data {
                let image = UIImage(data: data)
                completion?(image, nil)
            } else {
                print("Error while load image: \(String(describing: error?.localizedDescription))")
                completion?(nil, error)
            }
        }
        task.resume()
    }
    
}
