//
//  Article.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

extension Source {
    enum CodingKeys: String, CodingKey {
        case id, name
    }
}

class Source: Decodable {
    var id: String?
    var name: String?
    
    init(id: String?, name: String?) {
        self.id = id
        self.name = name
    }
}

protocol ArticleModel: Decodable, ArticleContentProvider, ArticleSourceProvider, ArticleImageProvider, ArticleDateProvider { }

extension Article: ArticleModel {
    enum CodingKeys: String, CodingKey {
        case source, author, title, description, url, urlToImage, publishedAt
    }
}

class Article: Decodable {
    
    var source: Source
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    
    init(source: Source, author: String?, title: String?, description: String?, url: String?, urlToImage: String?, publishedAt: String?) {
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
    }
}
