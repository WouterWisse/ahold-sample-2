//
//  Articles.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

extension Articles {
    enum CodingKeys: String, CodingKey {
        case status, totalResults, articles
    }
}

class Articles: Decodable {
    var status: String?
    var totalResults: Int?
    var articles: [Article]
    
    init(status: String?, totalResults: Int?, articles: [Article]) {
        self.status = status
        self.totalResults = totalResults
        self.articles = articles
    }
}
