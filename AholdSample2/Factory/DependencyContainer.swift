//
//  DependencyContainer.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 06/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

protocol ViewControllerFactory {
    func makeNewsCategoryViewController() -> NewsCategoryViewController
    func makeNewsTableViewController(apiService: NewsAPIService) -> NewsTableViewController 
    func makeArticleViewController(with article: Article) -> ArticleViewController
}

protocol NewsAPIServiceFactory {
    func makeDefaultAPIService() -> NewsAPIService
    func makeDefaultAPIService(with request: NewsAPIRequest) -> NewsAPIService
    func makeAPIService(baseURL: String, apiKey: String) -> NewsAPIService
}

protocol NewsAPIRequestFactory {
    func makeAPIRequest(section: String, country: String, category: NewsAPICategory) -> NewsAPIRequest
}

protocol ImageLoadFactory {
    func makeImageLoader(url: String) -> ImageLoader
}

class DependencyContainer {
}

// MARK: - ViewControllerFactory

extension DependencyContainer: ViewControllerFactory {
    
    func makeNewsCategoryViewController() -> NewsCategoryViewController {
        return NewsCategoryViewController(factory: self)
    }
    
    func makeNewsTableViewController(apiService: NewsAPIService) -> NewsTableViewController {
        return NewsTableViewController(factory: self, apiService: apiService)
    }
    
    func makeArticleViewController(with article: Article) -> ArticleViewController {
        return ArticleViewController(article: article, factory: self)
    }
}

// MARK: - NewsAPIServiceFactory

extension DependencyContainer: NewsAPIServiceFactory {

    func makeDefaultAPIService() -> NewsAPIService {
        return NewsAPIService(baseURL: "https://newsapi.org/v2/", apiKey: "baffd2d1de2a4d858985cce7e7d10407")
    }
    
    func makeDefaultAPIService(with request: NewsAPIRequest) -> NewsAPIService {
        let defaultAPIService = self.makeDefaultAPIService()
        defaultAPIService.request = request
        return defaultAPIService
    }
    
    func makeAPIService(baseURL: String = "https://newsapi.org/v2/", apiKey: String = "baffd2d1de2a4d858985cce7e7d10407") -> NewsAPIService {
        return NewsAPIService(baseURL: baseURL, apiKey: apiKey)
    }
}

// MARK: - NewsAPIRequestFactory

extension DependencyContainer: NewsAPIRequestFactory {
    
    func makeAPIRequest(section: String = "top-headlines", country: String = "nl", category: NewsAPICategory = .general) -> NewsAPIRequest {
        return NewsAPIRequest(section: section, country: country, category: category)
    }
}

// MARK: - ImageLoadFactory

extension DependencyContainer: ImageLoadFactory {
    
    func makeImageLoader(url: String) -> ImageLoader {
        return ImageLoader(imageURL: url)
    }
}
