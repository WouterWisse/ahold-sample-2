//
//  DepedencyContainerMock.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 07/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation


class DependencyContainerMock {
}

// MARK: - DependencyContainerMock

extension DependencyContainerMock: ViewControllerFactory {
    
    func makeNewsCategoryViewController() -> NewsCategoryViewController {
        return NewsCategoryViewController(factory: self)
    }
    
    func makeNewsTableViewController(apiService: NewsAPIService) -> NewsTableViewController {
        return NewsTableViewController(factory: self, apiService: apiService)
    }
    
    func makeArticleViewController(with article: Article) -> ArticleViewController {
        return ArticleViewController(article: article, factory: self)
    }
}

// MARK: - NewsAPIServiceFactory

extension DependencyContainerMock: NewsAPIServiceFactory {
    
    func makeDefaultAPIService() -> NewsAPIService {
        return NewsAPIServiceMock(baseURL: "", apiKey: "")
    }
    
    func makeDefaultAPIService(with request: NewsAPIRequest) -> NewsAPIService {
        let defaultAPIService = self.makeDefaultAPIService()
        defaultAPIService.request = request
        return defaultAPIService
    }
    
    func makeAPIService(baseURL: String, apiKey: String) -> NewsAPIService {
        return NewsAPIServiceMock(baseURL: baseURL, apiKey: apiKey)
    }
}

// MARK: - NewsAPIRequestFactory

extension DependencyContainerMock: NewsAPIRequestFactory {
    
    func makeAPIRequest(section: String = "top-headlines", country: String = "nl", category: NewsAPICategory = .general) -> NewsAPIRequest {
        return NewsAPIRequest(section: section, country: country, category: category)
    }
}

// MARK: - ImageLoadFactory

extension DependencyContainerMock: ImageLoadFactory {
    
    func makeImageLoader(url: String) -> ImageLoader {
        return ImageLoader(imageURL: url)
    }
}
