//
//  NewsAPIServiceMock.swift
//  AholdSample2
//
//  Created by Wouter Wisse on 07/08/2018.
//  Copyright © 2018 Wouter Wisse. All rights reserved.
//

import Foundation

class NewsAPIServiceMock: NewsAPIService {
    
    // MARK: - Internal Functions
    
    override func executeRequest(completion: RequestCompletionHandler?) {
        let url = Bundle.main.url(forResource: "mock", withExtension: "json")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if data != nil {
                let jsonDecoder = JsonDataDecoder<Articles>(data: data)
                do {
                    let articles = try jsonDecoder.loadData()
                    completion?(articles, nil)
                } catch {
                    print("Decoding json failed with error: \(error.localizedDescription)")
                    completion?(nil, error)
                }
            } else {
                print("Error while executing request: \(String(describing: error?.localizedDescription))")
                completion?(nil, error)
            }
        }
        task.resume()
    }
}
